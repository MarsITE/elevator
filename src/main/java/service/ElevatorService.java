package service;

import model.Direction;
import model.Elevator;
import model.Passenger;

import java.util.List;
import java.util.TreeMap;

public class ElevatorService {

    public Elevator nearest(Passenger passenger, List<Elevator> elevators) {
        TreeMap<Integer, Elevator> elevatorsWay = new TreeMap<>();

        for (Elevator elevator : elevators) {

            int relativePosition = Integer.compare(elevator.getCurrentFloor() - passenger.getCurrentFloor(), 0);        // relative position of the elevator and passenger
            int elevatorDirection = elevator.getToFloor() - elevator.getCurrentFloor();                                 // direction of elevator movement
            int optimalWay = Math.abs(elevator.getCurrentFloor() - passenger.getCurrentFloor());                        // optimal elevator path
            int way = Math.abs(elevator.getToFloor() - elevator.getCurrentFloor())
                    + Math.abs(elevator.getToFloor() - passenger.getCurrentFloor());                                    // suboptimal elevator path

            int trickyWay = switch (relativePosition) {
                case 1 -> (passenger.getDirection() == Direction.DOWN && elevatorDirection < 0)
                        || (elevatorDirection == 0) ? optimalWay : way;
                case -1 -> (passenger.getDirection() == Direction.UP && elevatorDirection > 0)
                        || (elevatorDirection == 0) ? optimalWay : way;
                case 0 -> (passenger.getDirection() == Direction.DOWN && elevatorDirection < 0)
                        || (passenger.getDirection() == Direction.UP && elevatorDirection > 0)
                        || (elevatorDirection == 0) ? 0 : way;
                default -> throw new IllegalStateException("Unexpected value: " + relativePosition);
            };

            elevatorsWay.put(trickyWay, elevator);
        }
        return elevatorsWay.get(elevatorsWay.firstKey());
    }
}
