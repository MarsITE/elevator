package model;

public class Elevator {
    private String name;
    private int currentFloor;
    private int toFloor;

    public Elevator(String name, int currentFloor, int toFloor) {
        this.name = name;
        this.currentFloor = currentFloor;
        this.toFloor = toFloor;
    }

    public String getName() {
        return name;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public int getToFloor() {
        return toFloor;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public void setToFloor(int toFloor) {
        this.toFloor = toFloor;
    }
}
