package model;

public class Passenger {
    private String name;
    private int currentFloor;
    private Direction direction;

    public Passenger(String name, int currentFloor, Direction direction) {
        this.name = name;
        this.currentFloor = currentFloor;
        this.direction = direction;
    }

    public String getName() {
        return name;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
