package model;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ModelTest {
    Passenger passenger;
    Elevator elevator;

    @BeforeMethod
    public void setUp() {
        passenger = new Passenger("Andrii", 9, Direction.DOWN);
        elevator = new Elevator("№1", 12, 10);
     }

    @Test
    public void testPassenger() {
        passenger.setName("Olga");
        passenger.setCurrentFloor(10);
        passenger.setDirection(Direction.UP);

        assertEquals(passenger.getName(), "Olga");
        assertEquals(passenger.getCurrentFloor(), 10);
        assertEquals(passenger.getDirection(), Direction.UP);
    }

    @Test
    public void testElevator() {
        elevator.setName("N3");
        elevator.setCurrentFloor(10);
        elevator.setToFloor(12);

        assertEquals(elevator.getName(), "N3");
        assertEquals(elevator.getCurrentFloor(), 10);
        assertEquals(elevator.getToFloor(), 12);
    }
}
