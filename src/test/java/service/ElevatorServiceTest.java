package service;

import model.Direction;
import model.Elevator;
import model.Passenger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

import static org.testng.Assert.*;

public class ElevatorServiceTest {
    ElevatorService elevatorService = new ElevatorService();

    @DataProvider(name = "data")
    public Object[][] createData() {
        Passenger passenger1 = new Passenger("Andrii", 9, Direction.DOWN);
        Passenger passenger2 = new Passenger("Vasyl", 10, Direction.UP);
        Passenger passenger3 = new Passenger("Olga", 12, Direction.DOWN);
        Passenger passenger4 = new Passenger("Viktor", 1, Direction.UP);

        Elevator elevator1 = new Elevator("№1", 12, 10);
        Elevator elevator2 = new Elevator("№2", 8, 9);
        Elevator elevator3 = new Elevator("№3", 7, 10);
        Elevator elevator4 = new Elevator("№4", 1, 12);
        Elevator elevator5 = new Elevator("№5", 12, 1);
        Elevator elevator6 = new Elevator("№6", 1, 1);

        List<Elevator> elevators1 = new LinkedList<>();
        elevators1.add(elevator1);
        elevators1.add(elevator2);

        List<Elevator> elevators2 = new LinkedList<>();
        elevators2.add(elevator1);
        elevators2.add(elevator3);

        List<Elevator> elevators3 = new LinkedList<>();
        elevators3.add(elevator4);
        elevators3.add(elevator5);

        List<Elevator> elevators4 = new LinkedList<>();
        elevators4.add(elevator6);
        elevators4.add(elevator5);

        return new Object[][]{{passenger1, elevators1, elevator2},
                              {passenger2, elevators2, elevator1},
                              {passenger3, elevators3, elevator5},
                              {passenger3, elevators4, elevator5},
                              {passenger4, elevators4, elevator6}};
    }

    @Test(dataProvider = "data")
    public void testNearest(Passenger passenger, List<Elevator> elevators, Elevator expected) {
        Elevator actual = elevatorService.nearest(passenger, elevators);
        assertEquals(actual, expected);
    }
}